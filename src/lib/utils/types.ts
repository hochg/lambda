import { APIGatewayProxyEvent, APIGatewayProxyResult } from 'aws-lambda';

type APIGatewayEventState = {
    state: {
        hellow: 'world';
    };
};

export type APIGatewayEvent = APIGatewayProxyEvent & APIGatewayEventState;
export type APIGatewayResult = APIGatewayProxyResult;
