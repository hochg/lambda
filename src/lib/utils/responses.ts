import { APIGatewayProxyResult } from 'aws-lambda';

const HEADERS = {
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Headers': '*',
    'Access-Control-Allow-Methods': '*',
};

export const success = (data: unknown): APIGatewayProxyResult => {
    return {
        statusCode: 200,
        headers: HEADERS,
        body: JSON.stringify(data),
    };
};
