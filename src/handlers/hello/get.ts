import { APIGatewayEvent, APIGatewayResult } from '@lib/utils/types';
import { logger } from '@lib/utils/logger';
import { success } from '@lib/utils/responses';

const handler = async (event: APIGatewayEvent): Promise<APIGatewayResult> => {
    logger.info(event);

    return success({ hello: 'world' });
};

exports.handler = handler;
